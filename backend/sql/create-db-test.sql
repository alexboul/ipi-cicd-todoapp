CREATE DATABASE IF NOT EXISTS `app_todolist_test` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER IF NOT EXISTS 'app_todolist_test'@'%' IDENTIFIED BY 'app_todolist_test';
GRANT ALL PRIVILEGES ON `app_todolist_test`.* TO 'app_todolist_test'@'%';
FLUSH PRIVILEGES;