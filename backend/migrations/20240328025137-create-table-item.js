'use strict';

var dbm;
var type;
var seed;

/**
 * We receive the dbmigrate dependency from dbmigrate initially.
 * This enables us to not have to rely on NODE_PATH.
 */
exports.setup = function (options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function (db, callback) {
  // table `item` - fields: id, name, done, updatedAt
  return db.createTable(
    'item',
    {
      id: { type: 'int', primaryKey: true, autoIncrement: true },
      name: { type: 'string', length: 100, notNull: true },
      done: { type: 'boolean', defaultValue: false },
      createdAt: { type: 'string', defaultValue: new Date().toISOString() },
      updatedAt: { type: 'string', defaultValue: new Date().toISOString() },
    },
    callback
  );
};

exports.down = function (db, callback) {
  return db.dropTable('item', callback);
};

exports._meta = {
  version: 1,
};
