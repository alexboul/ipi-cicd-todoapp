import { render, screen } from "@testing-library/react";
import {it, expect} from "vitest";
import {TodoListError} from "../TodoListError";


it('should display an error message', () => {
    const message = "Error message";

    render(<TodoListError message={message} />);

    expect(screen.getByText(message)).toBeInTheDocument();
})