import { render, screen } from "@testing-library/react";
import {it, expect} from "vitest";
import Footer from "../Footer";

it('should render with correct message', () => {
    const message = "Message from server";

    render(<Footer message={message} />);

    expect(screen.getByText(message)).toBeInTheDocument();
})

it('should render with correct error', () => {
    const error = { message: "Error message" };

    render(<Footer error={error} />);

    expect(screen.getByText(error.message)).toBeInTheDocument();
});