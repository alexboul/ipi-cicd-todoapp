import PropTypes from 'prop-types';

/**
 * Reçoit un message d'erreur et l'affiche.
 */
export function TodoListError({ message }) {
  return (
    <div className="TodoList-error">
      <p className="text-error">
        Error fetching tasks: <span className="text-bold">{message}</span>
      </p>
    </div>
  );
}

TodoListError.propTypes = {
  message: PropTypes.string.isRequired,
};
